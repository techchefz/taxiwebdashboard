import React, {Component} from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";
import PendingApproval from "./pendingApproval";
import RecentReview from "./recentReview";
import ActiveCustomers from "./activeCustomers";
import UserAction from "../../../redux/userDetails/action";
import tripAction from "../../../redux/trips/action";
import "./overview.scss";

class OverView extends Component {
  static propTypes = {
    recentReviewedTripList: PropTypes.object,
    fetchRecentReviewedTripDriverRiderDetails: PropTypes.func,
    fetchActiveCustomers: PropTypes.func,
    activeCustomersList: PropTypes.object
  };
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true
    };
    this.setLoading = this.setLoading.bind(this);
  }
  componentWillMount() {
    this.props.fetchRecentReviewedTripDriverRiderDetails();
    this.props.fetchActiveCustomers();
  }
  setLoading(loading) {
    this.setState({isLoading: loading});
  }
  render() {
    return (
      <div className="row animate trips">
        <div className="row">
          <PendingApproval />
          <RecentReview
            recentReviewList={this.props.recentReviewedTripList.data}
          />
        </div>
        <div className="row">
          <ActiveCustomers
            activeCustomersList={this.props.activeCustomersList.data}
          />
        </div>
      </div>
    );
  }
}
function mapStateToProps(state) {
  return {
    isLoggedIn: state.auth.isLoggedIn,
    loading: state.users.loading,
    meta: state.users.meta,
    recentReviewedTripList: state.trips.recentReviewedTripList,
    activeCustomersList: state.currentUser.activeCustomersList
  };
}
function bindActions(dispatch) {
  return {
    fetchRecentReviewedTripDriverRiderDetails: () =>
      dispatch(tripAction.fetchRecentReviewedTripDriverRiderDetails()),
    fetchActiveCustomers: () => dispatch(UserAction.fetchActiveCustomers())
  };
}
export default connect(mapStateToProps, bindActions)(OverView);
