import React, { Component } from "react";
import { connect } from "react-redux";
import { ListGroup, ListGroupItem } from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import PropTypes from "prop-types";
import UserAction from "../../../redux/userDetails/action";
import "./Overview.scss";

class PendingApproval extends Component {
  static propTypes = {
    approvePendingUsers: PropTypes.object,
    approveSelectedUser: PropTypes.func,
    rejectSelectedUser: PropTypes.func,
    fetchApprovePendingUsers: PropTypes.func,
    pendindusers: PropTypes.object
  };

  componentWillMount() {
    this.props.fetchApprovePendingUsers("driver");
  }

  openDetails(item) {
    this.props.setDetailView();
    this.props.sendDriverData(item);
  }


  render() {

    console.log('===========INSIDE PENDING APPROVAL=========================');
    console.log(this.props.approvePendingUsers);
    console.log('====================================');
    return (
      <div className="col-lg-4 col-md-6 col-sm-12 col-xs-12">
        <div className="panel panel-primary">
          <div className="panel-heading">
            {" "}
            <FormattedMessage
              id={"pending_approval"}
              defaultMessage="Pending Approval"
            />
          </div>
          <div className="panel-body">
            {this.props.approvePendingUsers.success ? (
              <ListGroup className="pannelListGroup">
                {this.props.approvePendingUsers.data.map(item => (
                  <ListGroupItem className="managedetails">
                    <ul>
                      <li className="user">
                        <div 
                          onClick={() => this.openDetails(item)}>
                          {item.fname} {item.lname}<br />
                          {item.email}
                        </div>
                      </li>
                    </ul>

                  </ListGroupItem>
                ))}
              </ListGroup>
            ) : (
                <span>
                  <FormattedMessage
                    id={"no_pending"}
                    defaultMessage="No Pending Approval list for Drivers"
                  />{" "}
                </span>
              )}
          </div>
        </div>
      </div>
    );
  }
}


function mapStateToProps(state) {
  return {
    mystate: state,
    pendindusers: state.currentUser.userApprovalInfo,
    approvePendingUsers: state.currentUser.approvePendingUsers
  };
}

function bindActions(dispatch) {
  return {
    approveSelectedUser: (id, userType) =>
      dispatch(UserAction.approveSelectedUser(id, userType)),
    rejectSelectedUser: (id, userType) =>
      dispatch(UserAction.rejectSelectedUser(id, userType)),
    fetchApprovePendingUsers: driver =>
      dispatch(UserAction.fetchApprovePendingUsers(driver)),
    setDetailView: () => dispatch(UserAction.setFullDriverDetails()),
    sendDriverData: (data) => dispatch(UserAction.sendSelectedDriverDetails(data))

  };
}
export default connect(mapStateToProps, bindActions)(PendingApproval);
