import React, { Component } from "react";
import PropTypes from "prop-types";
import { FormattedMessage } from "react-intl";
import { connect } from "react-redux";
import moment from 'moment';
import { Table, ButtonToolbar, Button } from "react-bootstrap";
import ViewImage from './modal';
import UserAction from "../../../redux/userDetails/action";
import "./Overview.scss";
import './details.css';
import dummy from '../../../resources/images/dummy.jpg'

class approvalOverview extends Component {
    static propTypes = {
        approvalOverviewList: PropTypes.array
    };

    constructor(props){
        super(props);
        this.state = {
            view: this.props.viewStatus,
            viewFullImage: false,
            url: null,
        }
        this.hideFullImage = this.hideFullImage.bind(this)
    }


    approveDriver(id, userType) {
        this.props.approveSelectedUser(id, userType);
    }
    rejectDriver(id, userType) {
        this.props.rejectSelectedUser(id, userType);
    }

    showFullImage(Source) {
        this.setState({
            ...this.state,
            url: Source,
            viewFullImage: true,
        })
    }
    hideFullImage() {
        this.setState({
            ...this.state,
            viewFullImage: false,
        })
    }

    render() {

        console.log('=============INSIDE BLA BLA BLA=======================');
        console.log(this.props);
        console.log('=============INSIDE BLA BLA BLA=======================');

        let PU = dummy;
        if (this.props.selectedDriverDetails.insuranceUrl !== null) {
            PU = this.props.selectedDriverDetails.insuranceUrl
        };
        let RCBU = dummy;
        if (this.props.selectedDriverDetails.rcBookUrl !== null) {
            RCBU = this.props.selectedDriverDetails.rcBookUrl
        };
        let VPU = dummy;
        if (this.props.selectedDriverDetails.vechilePaperUrl !== null) {
            VPU = this.props.selectedDriverDetails.vechilePaperUrl
        };
        let VFU = dummy;
        if (this.props.selectedDriverDetails.vechileFrontUrl !== null) {
            VFU = this.props.selectedDriverDetails.vechileFrontUrl
        };
        let VBU = dummy;
        if (this.props.selectedDriverDetails.vechileBackUrl !== null) {
            VBU = this.props.selectedDriverDetails.vechileBackUrl
        };

        let IURL = dummy;
        if (this.props.selectedDriverDetails !== undefined) {
            if (this.props.selectedDriverDetails.licenceDetails !== undefined) {
                if (this.props.selectedDriverDetails.licenceDetails.licenceUrl !== undefined) {
                    if (this.props.selectedDriverDetails.licenceDetails.licenceUrl !== null) {
                        IURL = this.props.selectedDriverDetails.licenceDetails.licenceUrl
                    }
                }
            };
        }

        if (this.props.selectedDriverDetails.bankDetails !== undefined) {
            if (this.props.selectedDriverDetails.bankDetails.IFSC === null) {
                this.props.selectedDriverDetails.bankDetails.IFSC = "N/A";
            }
            if (this.props.selectedDriverDetails.bankDetails.accountNo === null) {
                this.props.selectedDriverDetails.bankDetails.accountNo = "N/A";
            }
            if (this.props.selectedDriverDetails.bankDetails.holderName === null) {
                this.props.selectedDriverDetails.bankDetails.holderName = "N/A";
            }
        }

        if (this.props.selectedDriverDetails.licenceDetails !== undefined) {
            if (this.props.selectedDriverDetails.licenceDetails.licenceNo === null) {
                this.props.selectedDriverDetails.licenceDetails.licenceNo = "N/A";
            }
            if (this.props.selectedDriverDetails.licenceDetails.issueDate === null) {
                this.props.selectedDriverDetails.licenceDetails.issueDate = "N/A";
            } else {
                this.props.selectedDriverDetails.licenceDetails.issueDate = moment(this.props.selectedDriverDetails.licenceDetails.issueDate).format('MM/DD/YYYY');
            }
            if (this.props.selectedDriverDetails.licenceDetails.expDate === null) {
                this.props.selectedDriverDetails.licenceDetails.expDate = "N/A";
            } else {
                this.props.selectedDriverDetails.licenceDetails.expDate = moment(this.props.selectedDriverDetails.licenceDetails.expDate).format('MM/DD/YYYY');
            }
        }

        if (this.props.selectedDriverDetails.KBlicenceDetails !== undefined) {
            if (this.props.selectedDriverDetails.KBlicenceDetails.KBlicenceNo === null) {
                this.props.selectedDriverDetails.KBlicenceDetails.KBlicenceNo = "N/A";
            }
            if (this.props.selectedDriverDetails.KBlicenceDetails.KBReleaseDate === null) {
                this.props.selectedDriverDetails.KBlicenceDetails.KBReleaseDate = "N/A";
            } else {
                this.props.selectedDriverDetails.KBlicenceDetails.KBReleaseDate = moment(this.props.selectedDriverDetails.KBlicenceDetails.KBReleaseDate).format('MM/DD/YYYY');
            }
            if (this.props.selectedDriverDetails.KBlicenceDetails.KBrevisalDate === null) {
                this.props.selectedDriverDetails.KBlicenceDetails.KBrevisalDate = "N/A";
            } else {
                this.props.selectedDriverDetails.KBlicenceDetails.KBrevisalDate = moment(this.props.selectedDriverDetails.KBlicenceDetails.KBrevisalDate).format('MM/DD/YYYY');
            }
            if (this.props.selectedDriverDetails.KBlicenceDetails.KBexpDate === null) {
                this.props.selectedDriverDetails.KBlicenceDetails.KBexpDate = "N/A";
            } else {
                this.props.selectedDriverDetails.KBlicenceDetails.KBexpDate = moment(this.props.selectedDriverDetails.KBlicenceDetails.KBexpDate).format('MM/DD/YYYY');
            }
        }



        return (
            <div>
                {this.props.viewStatus === true ? (
                    <div className="col-lg-8 col-md-6 col-sm-12 col-xs-12" key={this.props.selectedDriverDetails._id}>
                        <div className="panel panel-primary">
                            <div className="panel-heading">
                                {" "}
                                <FormattedMessage
                                    id={"Overview-Details"}
                                    defaultMessage="OverView Details "
                                />
                            </div>
                            <div className="panel-body">
                                <div className="ManagePic">
                                    <div className="mpic" onClick={() => this.showFullImage(IURL)} ><img alt="icon" src={IURL} className="Pic" /><div>LICENCE</div> </div>
                                    <div className="mpic" onClick={() => this.showFullImage(PU)} ><img alt="icon" src={PU} className="Pic" /><div>INSURANCE</div> </div>
                                    <div className="mpic" onClick={() => this.showFullImage(RCBU)} ><img alt="icon" src={RCBU} className="Pic" /><div>RC BOOK</div> </div>
                                    <div className="mpic" onClick={() => this.showFullImage(VPU)} ><img alt="icon" src={VPU} className="Pic" /><div>VEHICLE PAPER</div> </div>
                                    <div className="mpic" onClick={() => this.showFullImage(VFU)} ><img alt="icon" src={VFU} className="Pic" /><div>VEHICLE FRONT</div> </div>
                                    <div className="mpic" onClick={() => this.showFullImage(VBU)} ><img alt="icon" src={VBU} className="Pic" /><div>VEHICLE BACK</div> </div>

                                </div>

                                <Table className="table1">
                                    <col width="180" />
                                    <th>User Details</th>
                                    <th></th>
                                    <tbody>

                                        <tr>
                                            <td>NAME</td>
                                            <td>{this.props.selectedDriverDetails.fname}{this.props.selectedDriverDetails.lname}</td>
                                        </tr>
                                        <tr>
                                            <td>Contact</td>
                                            <td>{this.props.selectedDriverDetails.phoneNo}</td>
                                        </tr>
                                        <tr>
                                            <td>E.Mail</td>
                                            <td>{this.props.selectedDriverDetails.email}</td>
                                        </tr>
                                    </tbody>
                                </Table>

                                <Table className="table1">
                                <thead>
                                    <th>Car Details</th>
                                    <th></th>
                                    <col width="180" />
                                        </thead>
                                    <tbody>
                                        <tr>
                                            <td>Owner Name</td>
                                            <td>{this.props.selectedDriverDetails.carDetails.RC_ownerName}</td>
                                        </tr>
                                        <tr>
                                            <td>Car Model</td>
                                            <td>{this.props.selectedDriverDetails.carDetails.carModel}</td>
                                        </tr>
                                        <tr>
                                            <td>Company</td>
                                            <td>{this.props.selectedDriverDetails.carDetails.company}</td>
                                        </tr>
                                        <tr>
                                            <td>Registration Date</td>
                                            <td>{moment(this.props.selectedDriverDetails.carDetails.regDate).format('MM/DD/YYYY')}</td>
                                        </tr>
                                        <tr>
                                            <td>Registration No</td>
                                            <td>{this.props.selectedDriverDetails.carDetails.regNo}</td>
                                        </tr>
                                        <tr>
                                            <td>Type</td>
                                            <td>{this.props.selectedDriverDetails.carDetails.type}</td>
                                        </tr>
                                        <tr>
                                            <td>Vehicle No</td>
                                            <td>{this.props.selectedDriverDetails.carDetails.vehicleNo}</td>
                                        </tr>
                                    </tbody>
                                </Table>

                                <div className="rowTable">
                                    <Table className="table1">
                                        <thead>
                                        <th>Licence Details</th>
                                        <th></th>
                                        <col width="180" />
                                            </thead>
                                        <tbody>
                                            <tr>
                                                <td>Licence No</td>
                                                <td>{this.props.selectedDriverDetails.licenceDetails.licenceNo}</td>
                                            </tr>
                                            <tr>
                                                <td>Release Date</td>
                                                <td>{this.props.selectedDriverDetails.licenceDetails.issueDate}</td>
                                            </tr>
                                            <tr>
                                                <td>Expiry Date</td>
                                                <td>{this.props.selectedDriverDetails.licenceDetails.expDate}</td>
                                            </tr>
                                        </tbody>
                                    </Table>
                                    <Table className="table1">
                                        <thead>
                                        <th>KB Licence Details</th>
                                        <th></th>
                                        <col width="180" />
                                            </thead>
                                        <tbody>
                                            <tr>
                                                <td>Licence No</td>
                                                <td>{this.props.selectedDriverDetails.KBlicenceDetails.KBlicenceNo}</td>
                                            </tr>
                                            <tr>
                                                <td>Release Date</td>
                                                <td>{this.props.selectedDriverDetails.KBlicenceDetails.KBReleaseDate}</td>
                                            </tr>
                                            <tr>
                                                <td>Expiry Date</td>
                                                <td>{this.props.selectedDriverDetails.KBlicenceDetails.KBexpDate}</td>
                                            </tr>
                                            <tr>
                                                <td>Revisal Date</td>
                                                <td>{this.props.selectedDriverDetails.KBlicenceDetails.KBrevisalDate}</td>
                                            </tr>
                                        </tbody>
                                    </Table>
                                </div>


                                <Table className="table1">
                                    <thead>
                                    <th>Account Details</th>
                                    <th></th>
                                    <col width="180" />
                                        </thead>
                                    <tbody>
                                        <tr>
                                            <td>Account Holder Name</td>
                                            <td>{this.props.selectedDriverDetails.bankDetails.holderName}</td>
                                        </tr>
                                        <tr>
                                            <td>Account No</td>
                                            <td>{this.props.selectedDriverDetails.bankDetails.accountNo}</td>
                                        </tr>
                                        <tr>
                                            <td>IFSC</td>
                                            <td>{this.props.selectedDriverDetails.bankDetails.IFSC}</td>
                                        </tr>
                                    </tbody>
                                </Table>
                                <span className="pannelListButtons Managebtn">
                                    <ButtonToolbar>
                                        <Button
                                            className="pannelListButtonGreen acceptbtn"
                                            onClick={() =>
                                                this.approveDriver(this.props.selectedDriverDetails._id, this.props.selectedDriverDetails.userType)
                                            }
                                        >
                                            <FormattedMessage
                                                id={"APPROVE"}
                                                defaultMessage="APPROVE"
                                            />
                                        </Button>
                                        <Button className="rejectbtn"
                                            onClick={() =>
                                                this.rejectDriver(this.props.selectedDriverDetails._id, this.props.selectedDriverDetails.userType)
                                            }
                                        >
                                            <FormattedMessage
                                                id={"REJECT"}
                                                defaultMessage="REJECT"
                                            />
                                        </Button>
                                    </ButtonToolbar>
                                </span>
                            </div>
                        </div>
                        <ViewImage view={this.state.viewFullImage} URL={this.state.url} handleHide={this.hideFullImage} />
                    </div>
                ) : (
                        null
                    )}
            </div>
        )
    }
}




function mapStateToProps(state) {
    return {
        viewStatus: state.currentUser.showDetailView,
        selectedDriverDetails: state.currentUser.notApproverdDriverDetails
    };
}

function bindActions(dispatch) {
    return {
        approveSelectedUser: (id, userType) =>
            dispatch(UserAction.approveSelectedUser(id, userType)),
        rejectSelectedUser: (id, userType) =>
            dispatch(UserAction.rejectSelectedUser(id, userType)),
        setDetailView: (data) => dispatch(UserAction.setFullDriverDetails(data)),
    };
}
export default connect(mapStateToProps, bindActions)(approvalOverview);