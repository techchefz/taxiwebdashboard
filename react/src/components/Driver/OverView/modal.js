import React, { Component } from 'react';
import { Modal, Button } from "react-bootstrap";
import './details.css';

export default class ViewImage extends Component {
    render() {
        return (
            <div>
                <Modal
                    {...this.props}
                    show={this.props.view}
                    onHide={this.props.handleHide}
                    dialogClassName="custom-modal"
                >
                    <Modal.Body className="text-center">
                       <Button className="closeButton" onClick={this.props.handleHide}>X</Button>

                        <img className="full-image" alt="icon" src={this.props.URL} />
                    </Modal.Body>
                </Modal>

            </div>
        )
    }
}
