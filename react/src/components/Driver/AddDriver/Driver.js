import React, { Component } from "react";
import { connect } from "react-redux";
import { Button } from "react-bootstrap";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import Toast from "../../Toast";
import UserAction from "../../../redux/users/action";
import PersonalInfo from "./personalInfo";
import LIcenceInfo from "./licenceinfo";
import VehicleInfo from "./vechileInfo";
import KBLIcenceInfo from './KBlicenceinfo';
import BankInfo from "./bankinfo";
import UploadDriverImages from './uploadDriverImages';

class AddDriver extends Component {
  static propTypes = {
    createUser: PropTypes.func,
    newUserObj: PropTypes.any,
    createUserErrorObj: PropTypes.any,
    failedCreateUserApi: PropTypes.any
  };
  constructor(props) {
    super(props);
    this.state = {
      userObj: {
        userType: "driver",
        fname: null,
        lname: null,
        password: null,
        email: null,
        phoneNo: null,
        dob: null,
        bloodGroup: null,
        tost: false,
        insuranceUrl: null,
        rcBookUrl: null,
        vechileBackUrl: null,
        vechileFrontUrl: null,
        vechilePaperUrl: null,

        licenceDetails: {
          licenceUrl: null,
          licenceNo: null,
          ReleaseDate: null,
          expDate: null
        },
        KBlicenceDetails: {
          KBlicenceNo: null,
          KBReleaseDate: null,
          KBexpDate: null,
          KBrevisalDate: null,
          KBlicenceUrl: null,
        },
        carDetails: {
          type: null,
          company: null,
          regNo: null,
          RC_ownerName: null,
          vehicleNo: null,
          carModel: null
        },
        bankDetails: {
          accountNo: null,
          holderName: null,
          IFSC: null
        }
      }
    };
    this.driverUpdate = this.driverUpdate.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.newUserObj.success) {
      this.props.history.push("/Home");
    }
  }
  driverUpdate = (name, val) => {
    const newObj = this.state.userObj;
    console.log(newObj, "hedskjvkdfvkdfbvkfhgbkfg");
    newObj[name] = val;
    this.setState({ userObj: newObj, toast: false });
    const userObj = this.state.userObj;
  };
  createDriver() {
    const userobj = this.state.userObj;

    this.props.createUser(userobj);
    this.setState.toast = true;
  }

  UrlsUpdate(data) {
    this.setState(prevState => ({
      userObj: {
        ...prevState.userObj,

        insuranceUrl: data.insuranceURL,
        rcBookUrl: data.rcBookUrl,
        vechileBackUrl: data.vechileBackUrl,
        vechileFrontUrl: data.vechileFrontUrl,
        vechilePaperUrl: data.vechilePaperUrl,
      }
    }));

    this.setState(prevState => ({
      userObj: {
        ...prevState.userObj,

        licenceDetails: {
          ...prevState.userObj.licenceDetails,
          licenceUrl: data.licenceURL
        },

        KBlicenceDetails: {
          ...prevState.userObj.KBlicenceDetails,
          KBlicenceUrl: data.KBlicenceURL
        }

      }
    }));
  }

  render() {
    console.log('====================================');
    console.log(this.state.userObj);
    console.log('====================================');
    return (
      <div className={"row animate"}>
        <div>
          {this.props.newUserObj.success && this.state.toast ? (
            <Toast
              message={this.props.newUserObj.message}
              showToast
              delay={1000}
            />
          ) : null}
          {!this.props.newUserObj.success && this.state.toast ? (
            <Toast message="user creation failed" showToast delay={1000} />
          ) : null}
        </div>
        <div className="row">
          <PersonalInfo
            personalobj={this.state.userObj}
            onUpdatePersonalInfo={this.driverUpdate}
          />
          <LIcenceInfo
            licenceobj={this.state.userObj}
            onUpdateLicenceInfo={this.driverUpdate}
          />
          <div className="row">
            <KBLIcenceInfo
              KBlicenceobj={this.state.userObj}
              onUpdateKBLicenceInfo={this.driverUpdate}
            />
            <VehicleInfo
              vehicleobj={this.state.userObj}
              onUpdateVehicleInfo={this.driverUpdate}
            />

          </div>

          <div className="row" >
            <UploadDriverImages
              updateUrls={this.UrlsUpdate.bind(this)}
            />
            <BankInfo
              bankobj={this.state.userObj}
              onUpdateBankInfo={this.driverUpdate}
            />
          </div>

          <div className="row">
            <Button
              style={{ display: "block", marginLeft: "50%" }}
              bsStyle="primary"
              onClick={() => this.createDriver()}
            >
              {" "}
              ADD DRIVER{" "}
            </Button>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    newUserLoading: state.users.newUserLoading,
    failedCreateUserApi: state.users.failedCreateUserApi,
    createUserErrorObj: state.users.createUserErrorObj,
    newUserObj: state.users.newUserObj
  };
}

function bindActions(dispatch) {
  return {
    createUser: userObj => dispatch(UserAction.createNewUser(userObj))
  };
}
export default withRouter(connect(mapStateToProps, bindActions)(AddDriver));
