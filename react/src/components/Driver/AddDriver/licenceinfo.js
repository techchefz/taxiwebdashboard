import React, {Component} from "react";
import {Form, FormGroup, ControlLabel, FormControl} from "react-bootstrap";
import PropTypes from "prop-types";
import {FormattedMessage} from "react-intl";
import "./personalInfo.scss";
import LicenceInfoForm from "./licenceInfoForm";

class LIcenceInfo extends Component {
  static propTypes = {
    licenceobj: PropTypes.object,
    onUpdateLicenceInfo: PropTypes.func
  };
  constructor(props) {
    super(props);
    this.state = {
      mm: null,
      dd: null,
      yyyy: null,
      licenceDetails: {
        licenceNo: null,
        ReleaseDate: null,
        expDate: null
      }
    };
  }
  handleChange(inputname, value, label) {
    if (inputname === "month" || inputname === "emonth") {
      this.setState({mm: value});
      const date = `${this.state.mm}/${this.state.dd}/${this.state.yyyy}`;
    } else if (inputname === "date" || inputname === "edate") {
      this.setState({dd: value});
      const date = `${this.state.mm}/${this.state.dd}/${this.state.yyyy}`;
    } else if (inputname === "year" || inputname === "eyear") {
      this.setState({yyyy: value});
      const date = `${this.state.mm}/${this.state.dd}/${value}`;

      label === "Id"
        ? (this.state.licenceDetails["ReleaseDate"] = date)
        : (this.state.licenceDetails["expDate"] = date);
      this.props.onUpdateLicenceInfo(
        "licenceDetails",
        this.state.licenceDetails
      );
    } else {
      this.state.licenceDetails["licenceNo"] = value;
      this.props.onUpdateLicenceInfo(
        "licenceDetails",
        this.state.licenceDetails
      );
    }
  }
  handleIssueDate(label) {
    const date = `${this.state.Imm}/${this.state.Idd}/${this.state.Iyyyy}`;
    this.props.onUpdateLicenceInfo(date, label, true);
  }
  handleExpDate(label) {
    const date = `${this.state.Emm}/${this.state.Edd}/${this.state.Eyyyy}`;
    this.props.onUpdateLicenceInfo(date, label, true);
  }
  render() {
    return (
      <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <div className="panel panel-primary adddriverpanel">
          <div className="panel-heading">
            <span>
              {" "}
              <FormattedMessage
                id={"license_info"}
                defaultMessage={"License Info"}
              />
            </span>
          </div>
          <div className="panel-body">
            <LicenceInfoForm
              OnformBlur={(name, val, label) =>
                this.handleChange(name, val, label)
              }
            />
          </div>
        </div>
      </div>
    );
  }
}
export default LIcenceInfo;
