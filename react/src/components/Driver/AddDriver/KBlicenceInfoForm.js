import React, { Component } from "react";
import PropTypes from "prop-types";
import { Field, reduxForm } from "redux-form";
import { FormattedMessage } from "react-intl";
import "./personalInfo.scss";

const validate = values => {
    const errors = {};
    if (!values.KBlicenceNo) {
        errors.KBlicenceNo = "Required";
    }
    return errors;
};

const renderField = ({
    input,
    label,
    placeholder,
    type,
    className,
    OnformBlur,
    meta: { touched, error, warning }
}) => (
        <div>
            {label === "Id" || label === "Ed" || label ==="Rd" ? (
                <span style={{ width: 0 }} />
            ) : (
                    <label className="col-md-4 col-lg-4 col-sm-4 formlabel"> {label}</label>
                )}
            <div>
                <input
                    className={className}
                    {...input}
                    type={type}
                    placeholder={placeholder}
                    onBlur={(e) => {
                        input.onBlur(e);
                        OnformBlur(input.name, input.value, label);
                    }}
                />
                {touched &&
                    ((error && (
                        <span
                            style={{
                                color: "red",
                                textAlign: "center",
                                display: "block"
                            }}
                        >
                            {error}
                        </span>
                    )))}
            </div>
        </div>
    );

class licenceInfoForm extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        handleSubmit: PropTypes.func,
        OnformBlur: PropTypes.func
    };
    constructor(props) {
        super(props);
        this.state = {
            isfetched: false
        };
    }
    render() {
        return (
            <form className="form">
                <div className="col-md-12 col-lg-12">
                    <div className="formdiv ">
                        <Field
                            className="col-md-8 col-lg-8 col-sm-8 formfield"
                            name="KBlicenceNo"
                            component={renderField}
                            type="text"
                            label={
                                <FormattedMessage id={"KB license"} defaultMessage={"KB License :"} />
                            }
                            placeholder="KB Licence No"
                            OnformBlur={this.props.OnformBlur}
                        />
                    </div>
                    <div className="formdiv ">
                        <label className="col-md-4 col-lg-4 col-sm-4 formlabel">
                            <FormattedMessage
                                id={"release_date"}
                                defaultMessage={"Release Date:"}
                            />
                        </label>
                        <span className="col-md-8 col-lg-8 col-sm-8 span1">
                            <Field
                                className="col-md-4 col-lg-4 col-sm-4 formcontroldd"
                                name="month"
                                label="Id"
                                component={renderField}
                                placeholder="MM"
                                OnformBlur={this.props.OnformBlur}
                            />
                            <Field
                                className="col-md-4 col-lg-4 col-sm-4 formcontrolmm"
                                name="date"
                                label="Id"
                                component={renderField}
                                placeholder="DD"
                                OnformBlur={this.props.OnformBlur}
                            />
                            <Field
                                className="col-md-4 col-lg-4 col-sm-4 formcontrolyy"
                                name="year"
                                label="Id"
                                component={renderField}
                                placeholder="YYYY"
                                OnformBlur={this.props.OnformBlur}
                            />
                        </span>
                    </div>
                    <div className="formdiv ">
                        <label className="col-md-4 col-lg-4 col-sm-4 formlabel">
                            <FormattedMessage id={"exp_date"} defaultMessage={"Exp Date:"} />
                        </label>
                        <span className="col-md-8 col-lg-8 col-sm-8 span1">
                            <Field
                                className="col-md-4 col-lg-4 col-sm-4 formcontroldd"
                                name="emonth"
                                label="Ed"
                                component={renderField}
                                placeholder="MM"
                                OnformBlur={this.props.OnformBlur}
                            />
                            <Field
                                className="col-md-4 col-lg-4 col-sm-4 formcontrolmm"
                                name="edate"
                                label="Ed"
                                component={renderField}
                                placeholder="DD"
                                OnformBlur={this.props.OnformBlur}
                            />
                            <Field
                                className="col-md-4 col-lg-4 col-sm-4 formcontrolyy"
                                name="eyear"
                                label="Ed"
                                component={renderField}
                                placeholder="YYYY"
                                OnformBlur={this.props.OnformBlur}
                            />
                        </span>
                    </div>
                    <div className="formdiv ">
                        <label className="col-md-4 col-lg-4 col-sm-4 formlabel">
                            <FormattedMessage
                                id={"revisal_date"}
                                defaultMessage={"Revisal Date:"}
                            />
                        </label>
                        <span className="col-md-8 col-lg-8 col-sm-8 span1">
                            <Field
                                className="col-md-4 col-lg-4 col-sm-4 formcontroldd"
                                name="rmonth"
                                label="Rd"
                                component={renderField}
                                placeholder="MM"
                                OnformBlur={this.props.OnformBlur}
                            />
                            <Field
                                className="col-md-4 col-lg-4 col-sm-4 formcontrolmm"
                                name="rdate"
                                label="Rd"
                                component={renderField}
                                placeholder="DD"
                                OnformBlur={this.props.OnformBlur}
                            />
                            <Field
                                className="col-md-4 col-lg-4 col-sm-4 formcontrolyy"
                                name="ryear"
                                label="Rd"
                                component={renderField}
                                placeholder="YYYY"
                                OnformBlur={this.props.OnformBlur}
                            />
                        </span>
                    </div>
                </div>
            </form>
        );
    }
}

export default reduxForm({
    form: "KBlicencedriver", // a unique identifier for this form
    validate, // <--- validation function given to redux-form
})(licenceInfoForm);
