import React, { Component } from "react";
import { Form, FormGroup, ControlLabel, FormControl } from "react-bootstrap";
import PropTypes from "prop-types";
import { FormattedMessage } from "react-intl";
import "./KBlicenceInfo.scss";
import KBLicenceInfoForm from "./KBlicenceInfoForm";

class KBLIcenceInfo extends Component {
    static propTypes = {
        KBlicenceobj: PropTypes.object,
        onUpdateKBLicenceInfo: PropTypes.func
    };
    constructor(props) {
        super(props);
        this.state = {
            mm: null,
            dd: null,
            yyyy: null,
            KBlicenceDetails: {
                KBlicenceNo: null,
                KBReleaseDate: null,
                KBexpDate: null,
                KBrevisalDate: null,
            }
        };
    }
    handleChange(inputname, value, label) {
        if (inputname === "month" || inputname === "emonth" || inputname === "rmonth") {
            this.setState({ mm: value });
            const date = `${this.state.mm}/${this.state.dd}/${this.state.yyyy}`;
        } else if (inputname === "date" || inputname === "edate" || inputname === "rdate") {
            this.setState({ dd: value });
            const date = `${this.state.mm}/${this.state.dd}/${this.state.yyyy}`;
        } else if (inputname === "year" || inputname === "eyear" || inputname === "ryear") {
            this.setState({ yyyy: value });
            const date = `${this.state.mm}/${this.state.dd}/${value}`;

            if (label === "Id") {
                this.state.KBlicenceDetails["KBReleaseDate"] = date
            } else if (label === "Ed") {
                this.state.KBlicenceDetails["KBexpDate"] = date
            } else if (label === "Rd") {
                this.state.KBlicenceDetails["KBrevisalDate"] = date
            }

            this.props.onUpdateKBLicenceInfo(
                "KBlicenceDetails",
                this.state.KBlicenceDetails
            );
        } else {
            this.state.KBlicenceDetails["KBlicenceNo"] = value;
            this.props.onUpdateKBLicenceInfo(
                "licenceDetails",
                this.state.KBlicenceDetails
            );
        }
    }
    handleIssueDate(label) {
        const date = `${this.state.Imm}/${this.state.Idd}/${this.state.Iyyyy}`;
        this.props.onUpdateKBLicenceInfo(date, label, true);
    }
    handleExpDate(label) {
        const date = `${this.state.Emm}/${this.state.Edd}/${this.state.Eyyyy}`;
        this.props.onUpdateKBLicenceInfo(date, label, true);
    }
    render() {

        return (
            <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div className="panel panel-primary adddriverpanel">
                    <div className="panel-heading">
                        <span>
                            {" "}
                            <FormattedMessage
                                id={" KB_license_info"}
                                defaultMessage={"KB License Info"}
                            />
                        </span>
                    </div>
                    <div className="panel-body">
                        <KBLicenceInfoForm
                            OnformBlur={(name, val, label) =>
                                this.handleChange(name, val, label)
                            }
                        />
                    </div>
                </div>
            </div>
        );
    }
}
export default KBLIcenceInfo;
