import React, { Component } from "react";
import { Form, FormGroup, ControlLabel, FormControl } from "react-bootstrap";
import PropTypes from "prop-types";
import { FormattedMessage } from "react-intl";
import "./personalInfo.scss";
import VehicleInfoForm from "./vehicleInfoForm";

class VehicleInfo extends Component {
  static propTypes = {
    vehicleobj: PropTypes.object,
    onUpdateVehicleInfo: PropTypes.func
  };
  constructor(props) {
    super(props);
    this.state = {
      st: null,
      stn: null,
      div: null,
      num: null,
      carDetails: {
        type: null,
        company: null,
        regNo: null,
        RC_ownerName: null,
        vehicleNo: null,
        carModel: null
      }
    };
  }
  handleChange(inputname, value, label) {
    if (inputname === "state") {
      this.setState({ st: value });
    } else if (inputname === "sno") {
      this.setState({ stn: value });
    } else if (inputname === "div") {
      this.setState({ div: value });
    } else if (inputname === "vehicleno") {
      this.setState({ num: value });
      const vno = this.state.st + this.state.stn + this.state.div + value;
      this.state.carDetails["vehicleNo"] = vno;
      this.props.onUpdateVehicleInfo("carDetails", this.state.carDetails);
    } else {
      this.state.carDetails[inputname] = value;
      this.props.onUpdateVehicleInfo("carDetails", this.state.carDetails);
    }
  }
  handleVehicleNo(label) {
    const vno =
      this.state.st + this.state.stn + this.state.div + this.state.num;
    this.props.onUpdateVehicleInfo(vno, label, true);
  }
  render() {
    return (
      <div className="col-lg-8 col-md-8 col-sm-12 col-xs-12">
        <div className="panel panel-primary adddriverpanel">
          <div className="panel-heading">
            <span>
              {" "}
              <FormattedMessage
                id={"vehicle_info"}
                defaultMessage={"Vehicle Info"}
              />
            </span>
          </div>
          <div className="panel-body">
            <VehicleInfoForm
              OnformBlur={(name, val, label) =>
                this.handleChange(name, val, label)
              }
            />
          </div>
        </div>
      </div>
    );
  }
}
export default VehicleInfo;
