import React, { Component } from "react";
import PropTypes from "prop-types";
import { FormattedMessage } from "react-intl";
import { connect } from "react-redux";
import moment from 'moment';
import { Table, ButtonToolbar, Button } from "react-bootstrap";
import ViewImage from './modal';
import "./Overview.scss";
import './details.css';
import dummy from '../../../../resources/images/dummy.jpg'

class Overview extends Component {

    constructor(props){
        super(props);
        this.state={
            viewFullImage: false,
            url: null,
        }    
        
        this.hideFullImage = this.hideFullImage.bind(this)
    }

    showFullImage(Source){
        this.setState({
            ...this.state,
            url: Source,
            viewFullImage: true,            
        })
    }
    hideFullImage(){
        this.setState({
            ...this.state,
            viewFullImage: false,
        })
    }

    render() {
        let PU = dummy;
        if (this.props.details.userprofiledetails.insuranceUrl !== null) {
            PU = this.props.details.userprofiledetails.insuranceUrl
        };
        let RCBU = dummy;
        if (this.props.details.userprofiledetails.rcBookUrl !== null) {
            RCBU = this.props.details.userprofiledetails.rcBookUrl
        };
        let VPU = dummy;
        if (this.props.details.userprofiledetails.vechilePaperUrl !== null) {
            VPU = this.props.details.userprofiledetails.vechilePaperUrl
        };
        let VFU = dummy;
        if (this.props.details.userprofiledetails.vechileFrontUrl !== null) {
            VFU = this.props.details.userprofiledetails.vechileFrontUrl
        };
        let VBU = dummy;
        if (this.props.details.userprofiledetails.vechileBackUrl !== null) {
            VBU = this.props.details.userprofiledetails.vechileBackUrl
        };

        let KBL= dummy;
        if (this.props.details.userprofiledetails !== undefined) {
            if (this.props.details.userprofiledetails.KBlicenceDetails !== undefined) {
                if (this.props.details.userprofiledetails.KBlicenceDetails.KBlicenceUrl !== undefined) {
                    if (this.props.details.userprofiledetails.KBlicenceDetails.KBlicenceUrl !== null) {
                        KBL = this.props.details.userprofiledetails.KBlicenceDetails.KBlicenceUrl
                    }
                }
            };
        }


        let IURL = dummy;
        if (this.props.details.userprofiledetails !== undefined) {
            if (this.props.details.userprofiledetails.licenceDetails !== undefined) {
                if (this.props.details.userprofiledetails.licenceDetails.licenceUrl !== undefined) {
                    if (this.props.details.userprofiledetails.licenceDetails.licenceUrl !== null) {
                        IURL = this.props.details.userprofiledetails.licenceDetails.licenceUrl
                    }
                }
            };
        }

        if (this.props.details.userprofiledetails.bankDetails !== undefined) {
            if (this.props.details.userprofiledetails.bankDetails.IFSC === null) {
                this.props.details.userprofiledetails.bankDetails.IFSC = "N/A";
            }
            if (this.props.details.userprofiledetails.bankDetails.accountNo === null) {
                this.props.details.userprofiledetails.bankDetails.accountNo = "N/A";
            }
            if (this.props.details.userprofiledetails.bankDetails.holderName === null) {
                this.props.details.userprofiledetails.bankDetails.holderName = "N/A";
            }
        }

        if (this.props.details.userprofiledetails.licenceDetails !== undefined) {
            if (this.props.details.userprofiledetails.licenceDetails.licenceNo === null) {
                this.props.details.userprofiledetails.licenceDetails.licenceNo = "N/A";
            }
            if (this.props.details.userprofiledetails.licenceDetails.issueDate === null) {
                this.props.details.userprofiledetails.licenceDetails.issueDate = "N/A";
            } else {
                this.props.details.userprofiledetails.licenceDetails.issueDate = moment(this.props.details.userprofiledetails.licenceDetails.issueDate).format('MM/DD/YYYY');
            }
            if (this.props.details.userprofiledetails.licenceDetails.expDate === null) {
                this.props.details.userprofiledetails.licenceDetails.expDate = "N/A";
            } else {
                this.props.details.userprofiledetails.licenceDetails.expDate = moment(this.props.details.userprofiledetails.licenceDetails.expDate).format('MM/DD/YYYY');
            }
        }



        return (
            <div>
                <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12" key={this.props.details.userprofiledetails._id}>
                    <div className="panel panel-primary">
                        <div className="panel-heading">
                            {" "}
                            <FormattedMessage
                                id={"Overview-Details"}
                                defaultMessage="Driver Details "
                            />
                        </div>
                        <div className="panel-body">
                            <div className="ManagePic">
                                <div className="mpic" onClick={() => this.showFullImage(IURL)}><img alt="icon" src={IURL} className="Pic" /><div>LICENCE</div> </div>
                                {/* <div className="mpic" onClick={() => this.showFullImage(KBL)}><img alt="icon" src={KBL} className="Pic" /><div>KB LICENCE</div> </div> */}
                                <div className="mpic" onClick={() => this.showFullImage(PU)}><img alt="icon" src={PU} className="Pic" /><div>INSURANCE</div> </div>
                                <div className="mpic" onClick={() => this.showFullImage(RCBU)}><img alt="icon" src={RCBU} className="Pic" /><div>RC BOOK</div> </div>
                                <div className="mpic" onClick={() => this.showFullImage(VPU)}><img alt="icon" src={VPU} className="Pic" /><div>VEHICLE PAPER</div> </div>
                                <div className="mpic" onClick={() => this.showFullImage(VFU)}><img alt="icon" src={VFU} className="Pic" /><div>VEHICLE FRONT</div> </div>
                                <div className="mpic" onClick={() => this.showFullImage(VBU)}><img alt="icon" src={VBU} className="Pic" /><div>VEHICLE BACK</div> </div>


                            </div>

                            <div className="divider">
                                <Table className="table1">
                                    <th>Car Details</th>
                                    <th></th>
                                    <col width="180" />
                                    {/* <col width="120" /> */}
                                    <tbody>
                                        <tr>
                                            <td>Owner Name</td>
                                            <td>{this.props.details.userprofiledetails.carDetails.RC_ownerName}</td>
                                        </tr>
                                        <tr>
                                            <td>Car Model</td>
                                            <td>{this.props.details.userprofiledetails.carDetails.carModel}</td>
                                        </tr>
                                        <tr>
                                            <td>Company</td>
                                            <td>{this.props.details.userprofiledetails.carDetails.company}</td>
                                        </tr>
                                        <tr>
                                            <td>Registration Date</td>
                                            <td>{moment(this.props.details.userprofiledetails.carDetails.regDate).format('MM/DD/YYYY')}</td>
                                        </tr>
                                        <tr>
                                            <td>Registration No</td>
                                            <td>{this.props.details.userprofiledetails.carDetails.regNo}</td>
                                        </tr>
                                        <tr>
                                            <td>Type</td>
                                            <td>{this.props.details.userprofiledetails.carDetails.type}</td>
                                        </tr>
                                        <tr>
                                            <td>Vehicle No</td>
                                            <td>{this.props.details.userprofiledetails.carDetails.vehicleNo}</td>
                                        </tr>
                                    </tbody>
                                </Table>
                                <Table className="table1">
                                    <th>KB Licence Details</th>
                                    <th></th>
                                    <col width="180" />
                                    <tbody>
                                        <tr>
                                            <td>Licence No</td>
                                            <td>{this.props.details.userprofiledetails.KBlicenceDetails.KBlicenceNo}</td>
                                        </tr>
                                        <tr>
                                            <td>Release Date</td>
                                            <td>{moment(this.props.details.userprofiledetails.KBlicenceDetails.KBReleaseDate).format('MM/DD/YYYY')}</td>
                                        </tr>
                                        <tr>
                                            <td>Expiry Date</td>
                                            <td>{moment(this.props.details.userprofiledetails.KBlicenceDetails.KBexpDate).format('MM/DD/YYYY')}</td>
                                        </tr>
                                        <tr>
                                            <td>Revisal Date</td>
                                            <td>{moment(this.props.details.userprofiledetails.KBlicenceDetails.KBrevisalDate).format('MM/DD/YYYY')}</td>
                                        </tr>
                                    </tbody>
                                </Table>

                            </div>

                            <div className="divider">
                                <Table className="table1">
                                    <th>Licence Details</th>
                                    <th></th>
                                    <col width="180" />
                                    <tbody>
                                        <tr>
                                            <td>Licence No</td>
                                            <td>{this.props.details.userprofiledetails.licenceDetails.licenceNo}</td>
                                        </tr>
                                        <tr>
                                            <td>Release Date</td>
                                            <td>{this.props.details.userprofiledetails.licenceDetails.issueDate}</td>
                                        </tr>
                                        <tr>
                                            <td>Expiry Date</td>
                                            <td>{this.props.details.userprofiledetails.licenceDetails.expDate}</td>
                                        </tr>
                                    </tbody>
                                </Table>
                                <Table className="table1">
                                    <th>Account Details</th>
                                    <th></th>
                                    <col width="180" />
                                    <tbody>
                                        <tr>
                                            <td>Account Holder Name</td>
                                            <td>{this.props.details.userprofiledetails.bankDetails.holderName}</td>
                                        </tr>
                                        <tr>
                                            <td>Account No</td>
                                            <td>{this.props.details.userprofiledetails.bankDetails.accountNo}</td>
                                        </tr>
                                        <tr>
                                            <td>IFSC</td>
                                            <td>{this.props.details.userprofiledetails.bankDetails.IFSC}</td>
                                        </tr>
                                    </tbody>
                                </Table>
                            </div>
                            <ViewImage view={this.state.viewFullImage} URL={this.state.url} handleHide={this.hideFullImage}/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default Overview;