import React, { Component } from "react";
import { Collapse, Well } from "react-bootstrap";
import "./styles.scss";
import { Link } from "react-router-dom";
import { FormattedMessage } from "react-intl";
const dashboardIcon = require("../../resources/images/dashboardIcons/dashboardIcon.png");
const driverIcon = require("../../resources/images/dashboardIcons/driver_icon.png");
const customerIcon = require("../../resources/images/dashboardIcons/customer_icon.png");
const appConfIcon = require("../../resources/images/dashboardIcons/app_confIcon.png");

class Sidebar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: 1,
      openDriverManagement: false,
      openCustomerManagement: false,
      openScheduleRide: false,
    };
  }

  render() {
    return (
      <aside className="sidebar">
        <div className="sidenav-outer sidenavOuter">
          <ul>
            <li
              className="sidemenuListTab"
              style={{
                backgroundColor:
                  this.state.activeTab === 1 ? "#1891e7" : "#5db2ee"
              }}
            >
              <Link
                to="/home"
                onClick={() => {
                  this.setState({
                    ...this.state,
                    activeTab: 1,
                    openCustomerManagement: false,
                    openDriverManagement: false,
                    openScheduleRide: false,
                  })
                }}
              >
                <img alt="icon" src={dashboardIcon} className="topnavImgLeft" />
                <span className="title">
                  <FormattedMessage
                    id={"dashboard"}
                    defaultMessage="Dashboard"
                  />
                </span>
              </Link>
            </li>
            <li
              className="sidemenuListTab"
              style={{
                backgroundColor:
                  this.state.activeTab === 2 ? "#1891e7" : "#5db2ee"
              }}
              data-toggle="collapse"
              data-target="#demo"
            >
              <a
                onClick={() => {
                  this.setState({
                    activeTab: 2,
                    openDriverManagement: !this.state.openDriverManagement,
                    openCustomerManagement: false,
                    openScheduleRide: false,
                  });
                }}
                style={{ cursor: "pointer" }}
              >
                <img alt="icon" src={driverIcon} className="topnavImgLeft" />
                <span className="title">
                  {" "}
                  <FormattedMessage
                    id={"driver_management"}
                    defaultMessage={"Driver Management"}
                  />
                </span>
                {this.state.openDriverManagement ? (
                  <span className="text-align-right glyphicon glyphicon-minus sidemenuListAddIcon" />
                ) : (
                    <span
                      className={
                        "text-align-right glyphicon glyphicon-plus sidemenuListAddIcon"
                      }
                    />
                  )}
              </a>
              <Collapse in={this.state.openDriverManagement}>
                <div>
                  <ul>
                    <li
                      className="sidemenuListTab"
                      style={{
                        backgroundColor:
                          this.state.activeTab === 2 ? "#1891e7" : "#5db2ee"
                      }}
                    >
                      <Link
                        to="/driver"
                        onClick={() => {
                          this.setState({
                            ...this.state,
                            activeTab: 2,
                          })
                        }}
                      >
                        <img
                          alt="icon"
                          src={customerIcon}
                          className="topnavImgLeft"
                        />
                        <span className="title">
                          {" "}
                          <FormattedMessage
                            id={"overview"}
                            defaultMessage={"Overview"}
                          />
                        </span>
                      </Link>
                    </li>
                    <li
                      className="sidemenuListTab"
                      style={{
                        backgroundColor:
                          this.state.activeTab === 2 ? "#1891e7" : "#5db2ee"
                      }}
                    >
                      <Link
                        to="/driver-list"
                        onClick={() => {
                          this.setState({
                            ...this.state,
                            activeTab: 2,
                          })
                        }}
                      >
                        <img
                          alt="icon"
                          src={appConfIcon}
                          className="topnavImgLeft"
                        />
                        <span className="title">
                          {" "}
                          <FormattedMessage
                            id={"drivers_list"}
                            defaultMessage={"Drivers List"}
                          />
                        </span>
                      </Link>
                    </li>
                    <li
                      className="sidemenuListTab"
                      style={{
                        backgroundColor:
                          this.state.activeTab === 2 ? "#1891e7" : "#5db2ee"
                      }}
                    >
                      <Link
                        to="/add-driver"
                        onClick={() => {
                          this.setState({
                            ...this.state,
                            activeTab: 2,
                          })
                        }}
                      >
                        <img
                          alt="icon"
                          src={appConfIcon}
                          className="topnavImgLeft"
                        />
                        <span className="tittle">
                          {" "}
                          <FormattedMessage
                            id={"add_driver"}
                            defaultMessage={"ADD DRIVER"}
                          />
                        </span>
                      </Link>
                    </li>
                  </ul>
                </div>
              </Collapse>
            </li>
            <li
              className="sidemenuListTab"
              style={{
                backgroundColor:
                  this.state.activeTab === 3 ? "#1891e7" : "#5db2ee"
              }}
            >
              <a
                onClick={() => {
                  this.setState({
                    activeTab:3,
                    openCustomerManagement: !this.state.openCustomerManagement,
                    openDriverManagement: false,
                    openScheduleRide: false,
                  });
                }}
                style={{ cursor: "pointer" }}
              >
                <img alt="icon" src={customerIcon} className="topnavImgLeft" />
                <span className="title">
                  <FormattedMessage
                    id={"customer_management"}
                    defaultMessage={"Customer Management"}
                  />
                </span>
                {this.state.openCustomerManagement ? (
                  <span
                    className={
                      "text-align-right glyphicon glyphicon-minus sidemenuListAddIcon"
                    }
                  />
                ) : (
                    <span
                      className={
                        "text-align-right glyphicon glyphicon-plus sidemenuListAddIcon"
                      }
                    />
                  )}
              </a>
              <Collapse in={this.state.openCustomerManagement}>
                <div>
                  <ul>
                    <li
                      className="sidemenuListTab"
                      style={{
                        backgroundColor:
                          this.state.activeTab === 3 ? "#1891e7" : "#5db2ee"
                      }}
                    >
                      <Link
                        to="/customer-list"
                        onClick={() => {
                          this.setState({
                            ...this.state,
                            activeTab: 3,
                          })
                        }}
                      >
                        <img
                          alt="icon"
                          src={appConfIcon}
                          className="topnavImgLeft"
                        />
                        <span className="title">
                          {" "}
                          <FormattedMessage
                            id={"customer_list"}
                            defaultMessage={"CUSTOMER LIST"}
                          />
                        </span>
                      </Link>
                    </li>
                    <li
                      className="sidemenuListTab"
                      style={{
                        backgroundColor:
                          this.state.activeTab === 3 ? "#1891e7" : "#5db2ee"
                      }}
                    >
                      <Link
                        to="/add-customer"
                        onClick={() => {
                          this.setState({
                            ...this.state,
                            activeTab: 3,
                          })
                        }}
                      >
                        <img
                          alt="icon"
                          src={appConfIcon}
                          className="topnavImgLeft"
                        />
                        <span className="title">
                          {" "}
                          <FormattedMessage
                            id={"add_customer"}
                            defaultMessage={"ADD CUSTOMER"}
                          />
                        </span>
                      </Link>
                    </li>
                  </ul>
                </div>
              </Collapse>
            </li>

            {/* =================================================================================== */}
            <li
              className="sidemenuListTab"
              style={{
                backgroundColor:
                  this.state.activeTab === 7 ? "#1891e7" : "#5db2ee"
              }}
            >
              <a
                onClick={() => {
                  this.state.activeTab = 7;
                  this.setState({
                    // openCustomerManagement: !this.state.openCustomerManagement,
                    openScheduleRide: !this.state.openScheduleRide,
                    openDriverManagement: false,
                    openCustomerManagement: false,
                  });
                }}
                style={{ cursor: "pointer" }}
              >
                <img alt="icon" src={customerIcon} className="topnavImgLeft" />
                <span className="title">
                  <FormattedMessage
                    id={"schedule_ride"}
                    defaultMessage={"SCHEDULED RIDES"}
                  />
                </span>
                {this.state.openScheduleRide ? (
                  <span
                    className={
                      "text-align-right glyphicon glyphicon-minus sidemenuListAddIcon"
                    }
                  />
                ) : (
                    <span
                      className={
                        "text-align-right glyphicon glyphicon-plus sidemenuListAddIcon"
                      }
                    />
                  )}
              </a>
              <Collapse in={this.state.openScheduleRide}>
                <div>
                  <ul>
                    <li
                      className="sidemenuListTab"
                      style={{
                        backgroundColor:
                          this.state.activeTab === 7 ? "#1891e7" : "#5db2ee"
                      }}
                    >
                      <Link
                        to="/pending-scheduled-rides"
                        onClick={() => {
                          this.setState({
                            activeTab: 7,
                          });
                        }}
                      >
                        <img
                          alt="icon"
                          src={appConfIcon}
                          className="topnavImgLeft"
                        />
                        <span className="title">
                          {" "}
                          <FormattedMessage
                            id={"pending_rides"}
                            defaultMessage={"PENDING RIDES"}
                          />
                        </span>
                      </Link>
                    </li>
                    <li
                      className="sidemenuListTab"
                      style={{
                        backgroundColor:
                          this.state.activeTab === 7 ? "#1891e7" : "#5db2ee"
                      }}
                    >
                      <Link
                        to="/completed-scheduled-rides"
                        onClick={() => {
                          this.setState({
                            activeTab: 7,
                          });
                        }}
                      >
                        <img
                          alt="icon"
                          src={appConfIcon}
                          className="topnavImgLeft"
                        />
                        <span className="title">
                          {" "}
                          <FormattedMessage
                            id={"completed_rides"}
                            defaultMessage={"COMPLETED RIDES"}
                          />
                        </span>
                      </Link>
                    </li>
                  </ul>
                </div>
              </Collapse>
            </li>
            {/* ============================================================================= */}

            <li
              className="sidemenuListTab"
              style={{
                backgroundColor:
                  this.state.activeTab === 4 ? "#1891e7" : "#5db2ee"
              }}
            >
              <Link
                to="/mobile-app-configuration"
                onClick={() => {
                  this.setState({
                    ...this.state,
                    activeTab: 4,
                    openCustomerManagement: false,
                    openDriverManagement: false,
                    openScheduleRide: false,
                  })

                }}
              >
                <img alt="icon" src={appConfIcon} className="topnavImgLeft" />
                <span className="title">
                  {" "}
                  <FormattedMessage
                    id={"mobile_config"}
                    defaultMessage={"MOBILE APP CONFIGURATION"}
                  />
                </span>
              </Link>
            </li>

            <li
              className="sidemenuListTab"
              style={{
                backgroundColor:
                  this.state.activeTab === 6 ? "#1891e7" : "#5db2ee"
              }}
            >
              <Link
                to="/server-configuration"
                onClick={() => {
                  this.setState({
                    ...this.state,
                    activeTab: 6,
                    openCustomerManagement: false,
                    openDriverManagement: false,
                    openScheduleRide: false,
                  })
                }}
              >
                <img alt="icon" src={appConfIcon} className="topnavImgLeft" />
                <span className="title">
                  {" "}
                  <FormattedMessage
                    id={"server_config"}
                    defaultMessage={"SERVER CONFIGURATION"}
                  />
                </span>
              </Link>
            </li>
          </ul>
        </div>
      </aside>
    );
  }
}

export default Sidebar;
