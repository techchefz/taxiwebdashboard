import React, { Component } from 'react'
import { connect } from "react-redux";
import moment from 'moment';
import tripAction from "../../../redux/trips/action";

import {
  Pagination,
  Button,
  FormControl,
  DropdownButton,
  MenuItem,
  InputGroup,
  Table
} from "react-bootstrap";
import './styles.css'
import { FormattedMessage } from "react-intl";
import ViewDetail from './modalCompleted';

class Completed extends Component {

  constructor(props) {
    super(props);
    this.state = {
      viewModal: false,
      item: null,
    }

    this.handleModal = this.handleModal.bind(this)
  }

  componentWillMount() {
    this.props.fetchAllScheduledRides();
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      viewModal: false,
      item: nextProps.scheduledRides
    })
  }


  handleModal(item) {
    this.setState(prevState => ({
      ...this.state,
      item: item,
      viewModal: !prevState.viewModal,
    }));
  }

  render() {
    return (
      <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div className="panel panel-primary">
          <div className="panel-heading panelheading">
            <span>
              {" "}
              <FormattedMessage
                id={"completed_rides"}
                defaultMessage={"Completed Rides"}
              />
            </span>

          </div>
          <div className="panel-body">
            <Table responsive>
              <thead>
                <tr>
                  <th className="col-lg-1">
                    <FormattedMessage
                      id={"trip_code"}
                      defaultMessage={"Trip Code"}
                    />
                  </th>
                  <th className="col-lg-4">
                    {" "}
                    <FormattedMessage
                      id={"start_from"}
                      defaultMessage={"Source Address"}
                    />
                  </th>
                  <th className="col-lg-3">
                    {" "}
                    <FormattedMessage
                      id={"end_at"}
                      defaultMessage={"Destination Address"}
                    />
                  </th>
                  <th className="col-lg-1">
                    {" "}
                    <FormattedMessage id={"Date"} defaultMessage={"Date"} />
                  </th>
                  <th className="col-lg-1">
                    {" "}
                    <FormattedMessage id={"time"} defaultMessage={"Time"} />
                  </th>
                  <th className="col-lg-2">
                    {" "}
                    <FormattedMessage
                      id={"user_name"}
                      defaultMessage={"Rider Name"}
                    />
                  </th>
                </tr>
              </thead>
              <tbody>
                {this.props.scheduledRides.length > 0 ?
                  this.props.scheduledRides.map((item, index) => (

                    item.tripRequestStatus == "completed" ? <tr key={index} onClick={() => this.handleModal(item)}>
                      <td>{item._id}</td>
                      <td>{item.pickUpAddress}</td>
                      <td>{item.destAddress}</td>
                      <td>{moment(item.scheduleOn).format('Do MMM YY')}</td>
                      <td>{moment(item.scheduleOn).format(' h:mm a')}</td>
                      <td>{item.riderFName} {item.riderLName}</td>
                    </tr> : null

                  )) : null}
              </tbody>
            </Table>


            {
              this.state.viewModal === true ?
                <ViewDetail viewhandler={() => this.handleModal()} item={this.state.item} />
                : null
            }


          </div>
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    scheduledRides: state.trips.allScheduledRides,
  };
}
function bindActions(dispatch) {
  return {
    fetchAllScheduledRides: () => {
      dispatch(tripAction.fetchAllScheduledRides());
    }
  }
}

export default connect(mapStateToProps, bindActions)(Completed);