import React, { Component } from 'react';
import Layout from '../../Layout/layout';
import Completed from './completedRides';

export default class CompletedScheduleRide extends Component {
    render() {
        return (
            <Layout>
               <Completed/>
            </Layout>
        )
    }
}
