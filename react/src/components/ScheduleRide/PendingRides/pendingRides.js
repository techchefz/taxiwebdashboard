import React, { Component } from "react";
import { connect } from "react-redux";
import {
    Pagination,
    Button,
    FormControl,
    DropdownButton,
    MenuItem,
    InputGroup,
    Table
} from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import moment from 'moment';
import tripAction from "../../../redux/trips/action";
import './styles.css'
import ViewDetail from './modalPending';


class Pending extends Component {

    constructor(props) {
        super(props);
        this.state = {
            viewModal: false,
            item: null,
        }

        this.handleModal = this.handleModal.bind(this),
            this.handleRide = this.handleRide.bind(this)
    }

    componentWillMount() {
        this.props.fetchAllScheduledRides();
    }

    componentWillReceiveProps(nextProps) {
        console.log(nextProps.scheduledRides)
        this.setState({
            viewModal: false,
            item: nextProps.scheduledRides
        })
    }

    handleRide(status) {
        if (status == "accept") {
            let reqObj = {
                _id: this.state.item._id,
                tripRequestStatus: "accept"
            }
            this.props.acceptScheduledRide(reqObj);
            this.setState({
                ...this.state,
                viewModal: false
            })
        }
        else if (status == "reject") {
            let reqObj = {
                _id: this.state.item._id,
                tripRequestStatus: "reject"
            }
            this.props.rejectScheduledRide(reqObj);
            this.setState({
                ...this.state,
                viewModal: false
            })
        }
    }



    handleModal(item) {
        this.setState(prevState => ({
            ...this.state,
            item: item,
            viewModal: !prevState.viewModal,
        }));
    }

    render() {

        return (
            <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div className="panel panel-primary">
                    <div className="panel-heading panelheading">
                        <span>
                            {" "}
                            <FormattedMessage
                                id={"pending_rides"}
                                defaultMessage={"Pending Rides"}
                            />
                        </span>

                    </div>
                    <div className="panel-body">
                        <Table responsive>
                            <thead>
                                <tr>
                                    <th className="col-lg-1">
                                        <FormattedMessage
                                            id={"trip_code"}
                                            defaultMessage={"Trip Code"}
                                        />
                                    </th>
                                    <th className="col-lg-4">
                                        {" "}
                                        <FormattedMessage
                                            id={"start_from"}
                                            defaultMessage={"Source Address"}
                                        />
                                    </th>
                                    <th className="col-lg-3">
                                        {" "}
                                        <FormattedMessage
                                            id={"end_at"}
                                            defaultMessage={"Destination Address"}
                                        />
                                    </th>
                                    <th className="col-lg-1">
                                        {" "}
                                        <FormattedMessage id={"Date"} defaultMessage={"Date"} />
                                    </th>
                                    <th className="col-lg-1">
                                        {" "}
                                        <FormattedMessage id={"time"} defaultMessage={"Time"} />
                                    </th>
                                    <th className="col-lg-2">
                                        {" "}
                                        <FormattedMessage
                                            id={"user_name"}
                                            defaultMessage={"Rider Name"}
                                        />
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.props.scheduledRides.length > 0 ?
                                    this.props.scheduledRides.map((item, index) => (

                                        item.tripRequestStatus == "request" ?
                                            <tr key={index} onClick={() => this.handleModal(item)}>
                                                <td>{item._id}</td>
                                                <td>{item.pickUpAddress}</td>
                                                <td>{item.destAddress}</td>
                                                <td>{moment(item.scheduleOn).format('Do MMM YY')}</td>
                                                <td>{moment(item.scheduleOn).format(' h:mm a')}</td>
                                                <td>{item.riderFName} {item.riderLName}</td>
                                            </tr> : null
                                    )) : null}
                            </tbody>
                        </Table>


                        {
                            this.state.viewModal === true ?
                                <ViewDetail viewhandler={() => this.handleModal()} item={this.state.item} handleRide={this.handleRide} />
                                : null
                        }


                    </div>
                </div>
            </div>
        )
    }
}
function mapStateToProps(state) {
    return {
        scheduledRides: state.trips.allScheduledRides,
    };
}
function bindActions(dispatch) {
    return {
        fetchAllScheduledRides: () => {
            dispatch(tripAction.fetchAllScheduledRides());
        },
        acceptScheduledRide: (payload) => {
            dispatch(tripAction.acceptScheduledRide(payload));
        },
        rejectScheduledRide: (payload) => {
            dispatch(tripAction.rejectScheduledRide(payload));
        }
    }
}

export default connect(mapStateToProps, bindActions)(Pending);
