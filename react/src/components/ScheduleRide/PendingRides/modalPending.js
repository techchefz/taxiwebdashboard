import React, { Component } from 'react';
import { Modal, Button } from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import moment from 'moment';

import './styles.css';

export default class ViewDetails extends Component {
    render() {
        return (
            <div>
                <Modal
                    {...this.props}
                    show={true}
                    onHide={this.props.handleHide}
                    dialogClassName="custom-modal"
                >
                    <Modal.Body className="text-center">
                        <div className="manageDetails">
                        <table className="panelTable card">
                            <thead>
                                <tr className="panelTableHead">
                                    <th className="col-md-1">RIDE ID :</th>
                                    <th className="col-md-2">
                                        {" "}
                                            <FormattedMessage id={"TripID"} defaultMessage={this.props.item._id} />
                                        <Button className="closeButton1" onClick={this.props.viewhandler}>Cancel</Button>
                                    </th>
                                </tr>
                            </thead>
                            <tbody className="panelTableTBody">
                                <tr>
                                    <td>PickUp :</td>
                                        <td>{this.props.item.pickUpAddress}</td>
                                </tr>
                                <tr>
                                    <td>Destination :</td>
                                        <td>{this.props.item.destAddress}</td>
                                </tr>
                                <tr>
                                    <td>Date :</td>
                                        <td>{moment(this.props.item.scheduleOn).format('Do MMM YY')}</td>
                                </tr>
                                <tr>
                                    <td>Time :</td>
                                        <td>{moment(this.props.item.scheduleOn).format(' h:mm a')}</td>
                                </tr>
                                <tr>
                                    <td>Passengers :</td>
                                        <td>{this.props.item.numberOfPassengers}</td>
                                </tr>
                                <tr>
                                    <td><p></p></td>
                                </tr>
                                <tr>
                                    <td>Rider Id :</td>
                                        <td>{this.props.item.riderId}</td>
                                </tr>
                                <tr>
                                    <td>Name :</td>
                                        <td>{this.props.item.riderFName} {this.props.item.riderLName}</td>
                                </tr>
                                <tr>
                                    <td>Phone :</td>
                                        <td>{this.props.item.riderPhoneNo}</td>
                                </tr>
                                <tr>
                                    <td>EMail :</td>
                                        <td>{this.props.item.riderEmail}</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><Button
                                        style={{ display: "block", float: "left" , marginRight: 10 }}
                                        bsStyle="primary"
                                            onClick={() => this.props.handleRide("accept")}
                                    >
                                        {" "}
                                        <FormattedMessage
                                            id={"accept_ride"}
                                            defaultMessage={"ACCEPT"}
                                        />
                                    </Button>
                                        <Button
                                            style={{ display: "block" }}
                                            bsStyle="primary"
                                                onClick={() => this.props.handleRide("reject")}
                                        >
                                            {" "}
                                            <FormattedMessage
                                                id={"reject_ride"}
                                                defaultMessage={"REJECT"}
                                            />
                                        </Button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                    </Modal.Body>
                </Modal>

            </div>
        )
    }
}
