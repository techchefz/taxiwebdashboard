import React, { Component } from 'react';
import Layout from '../../Layout/layout';
import Pending from './pendingRides';

export default class PendingScheduleRide extends Component {
    render() {
        return (
            <Layout>
                <Pending />
            </Layout>
        )
    }
}
