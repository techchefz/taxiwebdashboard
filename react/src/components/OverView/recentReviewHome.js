import React, {Component} from "react";
import moment from "moment";
import PropTypes from "prop-types";
import {FormattedMessage} from "react-intl";
import RecentReviewRating from "./recentReviewRating";

export default class RecentReviewHome extends Component {
  static propTypes = {
    recentReviewedTripDetails: PropTypes.array
  };
  timeformat(time) {
    const d = time.substring(11, 16);
    return moment(d, "h:mm a").fromNow();
  }

  render() {
    return (
      <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <div className="panel panel-primary">
          <div className="panel-heading">
            {" "}
            <FormattedMessage
              id={"recent_review"}
              defaultMessage={"Recent Review"}
            />
          </div>
          <div className="panel-body">
            {this.props.recentReviewedTripDetails ? (
              this.props.recentReviewedTripDetails.map(item => (
                <div>
                  <div />
                  <div>
                    <div>
                      <span>{item.driverName}</span>
                      <RecentReviewRating
                        noofstars={5}
                        ratedStar={item.driverRatingByRider}
                      />
                      <span>{this.timeformat(item.bookingTime)}</span>
                    </div>
                    <p>{item.driverReviewByRider}</p>
                  </div>
                </div>
              ))
            ) : (
              <p>
                <FormattedMessage
                  id={"no_review"}
                  defaultMessage={"No Review"}
                />
              </p>
            )}
          </div>
        </div>
      </div>
    );
  }
}
