import React, {Component} from "react";
import {Table} from "react-bootstrap";
import {FormattedMessage} from "react-intl";
export default class OnGoingTrips extends Component {
  render() {
    return (
      <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div className="panel panel-primary">
          <div className="panel-heading">
            {" "}
            <FormattedMessage
              id={"ongoing_trips"}
              defaultMessage={"Ongoing Trips"}
            />
          </div>
          <div className="panel-body">
            <Table responsive>
              <thead>
                <tr>
                  <th className="col-lg-1">
                    <FormattedMessage
                      id={"trip_code"}
                      defaultMessage={"Trip Code"}
                    />
                  </th>
                  <th className="col-lg-4">
                    {" "}
                    <FormattedMessage
                      id={"start_from"}
                      defaultMessage={"Starting From"}
                    />
                  </th>
                  <th className="col-lg-1">
                    {" "}
                    <FormattedMessage id={"time"} defaultMessage={"Time"} />
                  </th>
                  <th className="col-lg-3">
                    {" "}
                    <FormattedMessage
                      id={"driver_name"}
                      defaultMessage={"Driver Name"}
                    />
                  </th>
                  <th className="col-lg-3">
                    {" "}
                    <FormattedMessage
                      id={"user_name"}
                      defaultMessage={"User Name"}
                    />
                  </th>
                </tr>
              </thead>
              <tbody>
                {this.props.ongoingTripDetails
                  ? this.props.ongoingTripDetails.map((item, index) => (
                    <tr key={index}>
                      <td>{item._id}</td>
                      <td>{item.pickUpAddress}</td>
                      <td>
                        {item.bookingTime.substring(11, 16)}{" "}
                        {parseInt(item.bookingTime.substring(11, 14)) < 11 ? (
                          <span>AM </span>
                        ) : (
                          <span>PM</span>
                        )}
                      </td>
                      <td>{item.driverName}</td>
                      <td>{item.riderName}</td>
                    </tr>
                  ))
                  : null}
              </tbody>
            </Table>
          </div>
        </div>
      </div>
    );
  }
}
