export default {
  // local server
  // serverUrl: "http://192.168.1.14", // use Ip address instead of localhost
  // port: 3010 //incase of running api-server (node app) in production
  serverUrl: "http://159.89.3.128", // use Ip address instead of localhost
  port: 3066 //incase of running api-server (node app) in production
  //port: 3010 // incase of running api-server (node app) in development
  //  port: 443 //incase of running api-server (node app) on heroku
};
