import config from "./apiConfig.js";
// import {storeObj} from '../store/domain';

const io = require("socket.io-client");
let socket = null;
export function socketDriverInit() {
  console.log("store heree");
  socket = io(`${config.serverUrl}:${config.port}`, {
    jsonp: false,
    transports: ["websocket"],
    query: "token=JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyIkX18iOnsic3RyaWN0TW9kZSI6dHJ1ZSwic2VsZWN0ZWQiOnt9LCJnZXR0ZXJzIjp7fSwid2FzUG9wdWxhdGVkIjpmYWxzZSwiYWN0aXZlUGF0aHMiOnsicGF0aHMiOnsicGFzc3dvcmQiOiJpbml0IiwiZW1haWwiOiJpbml0IiwiZm5hbWUiOiJpbml0IiwibG5hbWUiOiJpbml0IiwicmVjb3ZlcnlFbWFpbCI6ImluaXQiLCJkb2IiOiJpbml0IiwiYWRkcmVzcyI6ImluaXQiLCJjaXR5IjoiaW5pdCIsInN0YXRlIjoiaW5pdCIsImNvdW50cnkiOiJpbml0IiwiZW1lcmdlbmN5RGV0YWlscyI6ImluaXQiLCJsYXRpdHVkZURlbHRhIjoiaW5pdCIsImxvbmdpdHVkZURlbHRhIjoiaW5pdCIsInVzZXJSYXRpbmciOiJpbml0IiwicGhvbmVObyI6ImluaXQiLCJwcm9maWxlVXJsIjoiaW5pdCIsImN1cnJUcmlwSWQiOiJpbml0IiwiY3VyclRyaXBTdGF0ZSI6ImluaXQiLCJ1c2VyVHlwZSI6ImluaXQiLCJsb2dpblN0YXR1cyI6ImluaXQiLCJob21lQWRkcmVzcyI6ImluaXQiLCJ3b3JrQWRkcmVzcyI6ImluaXQiLCJ2ZXJpZmllZCI6ImluaXQiLCJqd3RBY2Nlc3NUb2tlbiI6ImluaXQiLCJjYXJEZXRhaWxzIjoiaW5pdCIsImRldmljZUlkIjoiaW5pdCIsInB1c2hUb2tlbiI6ImluaXQiLCJncHNMb2MiOiJpbml0IiwiX2lkIjoiaW5pdCJ9LCJzdGF0ZXMiOnsiaWdub3JlIjp7fSwiZGVmYXVsdCI6e30sImluaXQiOnsiZm5hbWUiOnRydWUsImxuYW1lIjp0cnVlLCJyZWNvdmVyeUVtYWlsIjp0cnVlLCJkb2IiOnRydWUsImFkZHJlc3MiOnRydWUsImNpdHkiOnRydWUsInN0YXRlIjp0cnVlLCJjb3VudHJ5Ijp0cnVlLCJlbWVyZ2VuY3lEZXRhaWxzIjp0cnVlLCJsYXRpdHVkZURlbHRhIjp0cnVlLCJsb25naXR1ZGVEZWx0YSI6dHJ1ZSwidXNlclJhdGluZyI6dHJ1ZSwicGhvbmVObyI6dHJ1ZSwicHJvZmlsZVVybCI6dHJ1ZSwiY3VyclRyaXBJZCI6dHJ1ZSwiY3VyclRyaXBTdGF0ZSI6dHJ1ZSwibG9naW5TdGF0dXMiOnRydWUsImhvbWVBZGRyZXNzIjp0cnVlLCJ3b3JrQWRkcmVzcyI6dHJ1ZSwidmVyaWZpZWQiOnRydWUsImp3dEFjY2Vzc1Rva2VuIjp0cnVlLCJjYXJEZXRhaWxzIjp0cnVlLCJkZXZpY2VJZCI6dHJ1ZSwicHVzaFRva2VuIjp0cnVlLCJncHNMb2MiOnRydWUsInVzZXJUeXBlIjp0cnVlLCJwYXNzd29yZCI6dHJ1ZSwiZW1haWwiOnRydWUsIl9pZCI6dHJ1ZX0sIm1vZGlmeSI6e30sInJlcXVpcmUiOnt9fSwic3RhdGVOYW1lcyI6WyJyZXF1aXJlIiwibW9kaWZ5IiwiaW5pdCIsImRlZmF1bHQiLCJpZ25vcmUiXX0sImVtaXR0ZXIiOnsiZG9tYWluIjpudWxsLCJfZXZlbnRzIjp7InNhdmUiOltudWxsLG51bGwsbnVsbCxudWxsXSwiaXNOZXciOltudWxsLG51bGwsbnVsbCxudWxsXX0sIl9ldmVudHNDb3VudCI6MiwiX21heExpc3RlbmVycyI6MH19LCJpc05ldyI6ZmFsc2UsIl9kb2MiOnsiZm5hbWUiOm51bGwsImxuYW1lIjpudWxsLCJyZWNvdmVyeUVtYWlsIjoiZ2Vla3lhbnRzQHNhaHVzb2Z0LmNvbSIsImRvYiI6IjE5NDktMTItMzFUMTg6MzA6MDAuMDAwWiIsImFkZHJlc3MiOiJhYmNkIiwiY2l0eSI6ImJhbmdhbG9yZSIsInN0YXRlIjoia2FybmF0YWthIiwiY291bnRyeSI6ImluZGlhIiwiZW1lcmdlbmN5RGV0YWlscyI6W10sImxhdGl0dWRlRGVsdGEiOjAuMDEzLCJsb25naXR1ZGVEZWx0YSI6MC4wMjIsInVzZXJSYXRpbmciOjAsInBob25lTm8iOiIxMjM0NTY3ODkwIiwicHJvZmlsZVVybCI6bnVsbCwiY3VyclRyaXBJZCI6bnVsbCwiY3VyclRyaXBTdGF0ZSI6bnVsbCwidXNlclR5cGUiOiJhZG1pbiIsImxvZ2luU3RhdHVzIjp0cnVlLCJob21lQWRkcmVzcyI6bnVsbCwid29ya0FkZHJlc3MiOm51bGwsInZlcmlmaWVkIjowLCJqd3RBY2Nlc3NUb2tlbiI6bnVsbCwiY2FyRGV0YWlscyI6W10sImRldmljZUlkIjpudWxsLCJwdXNoVG9rZW4iOm51bGwsImdwc0xvYyI6WzE5LjAyMTcyOTAyMzU0NTE1LDcyLjg1MzY4MjczMzA4NTQ1XSwicGFzc3dvcmQiOiIkMmEkMTAkYllFMmJtVWlKcnNmczJmYnFWanJITzhBc3FJVVNYN0RFMWp2SkFwNjE0cUh1Y1M4dGh2ei4iLCJlbWFpbCI6ImFkbWluQGdlZWt5YW50cy5jb20iLCJfaWQiOiI1OTQyOGIxYmIwYzNjYzBmNTU0ZmQ1MmEifSwiJGluaXQiOnRydWUsImlhdCI6MTQ5NzYxOTMwNX0.5QBs7nkJuXECn0_-rEqq1WfH7nXUY525Z2wX0PNgKUE"
    //${store.getState().user.jwtAccessToken}`,
  });
  socket.on("connect", () => {
    console.log("connected");
    // socket.emit('getDriverDetails', data);
  });
  socket.on("getDriverDetails", data => {
    console.log("getdriver", data);
  });
  // socket.emit('getDriverDetails');
  socket.on("disconnect", () => {
    console.log("disconnected");
  });
  // socket.on('responseTimedOut', () => {
  //   console.log('timeout');
  // });
  // socket.on('updateDriverLocation', gpsLoc => {
  //   console.log('gps loc', gpsLoc);
  // });
  // socket.on('updateLocation', gpsLoc => {
  //   console.log('updated loc', gpsLoc);
  // });
  // socket.on('socketError', e => {
  //   alert(e);
  // });
}

// export function updateLocation(user) {
//   socket.emit('updateLocation', user);
// }
